//
//

#include "key_reader.h"

#define BUFFER 0
#define BUFFER_AMOUNT 2

KeyReader::~KeyReader() = default;

double KeyReader::read_voltage() {
    if (BUFFER) {
        double buffer_sum = 0;
        for (int i = 0; i < BUFFER_AMOUNT; i++)
            buffer_sum += readerIn.read();
        last_voltage = buffer_sum / BUFFER_AMOUNT;
    } else {
        last_voltage = readerIn.read();
    }

    return last_voltage;
}

double KeyReader::read_frequency() {
    read_voltage();
    last_freq = get_freq_step((int) (11 * last_voltage));
    return last_freq;
}

double KeyReader::get_freq_step(int i) {
    return steps[i];
}

double KeyReader::get_frequency() {
    last_freq = get_freq_step((int) (11 * last_voltage));
    return last_freq;
}
