//
//

#ifndef FINALPROJECT_KEY_READER_H
#define FINALPROJECT_KEY_READER_H

#include "mbed.h"
#include <cmath>

#define STRIP_HI (double) 1.0
#define STRIP_LO (double) .0045
#define TWO_ROOT_TWELVE 1.05946309436

class KeyReader{

public:
    KeyReader(PinName pin, double base_frequency) : readerIn(pin), base_frequency(base_frequency) {
        last_voltage = 0;
        last_freq = 0;
        for(int i = 0; i < 12; i++){
            steps[i] = base_frequency * pow(TWO_ROOT_TWELVE, i);
        }
    }

    ~KeyReader();

    double read_voltage();
    double read_frequency();
    double get_last_voltage(){
        return last_voltage;
    }
    double get_last_freq(){
        return last_freq;
    }

    double get_freq_step(int i);

    double get_frequency();

private:
    AnalogIn readerIn;
    double base_frequency;
    double last_voltage;
    double last_freq;

    double steps[12];

};

#endif //FINALPROJECT_KEY_READER_H
