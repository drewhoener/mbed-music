#!/bin/bash

cd ~/Projects/CLionProjects/FinalProject
mbed compile --profile .temp/tools/profiles/debug.json

rm ~/extern/MBED/*.bin
cd ~/Projects/CLionProjects/FinalProject/BUILD/LPC1768/GCC_ARM-DEBUG/
cp FinalProject.bin ~/extern/MBED
