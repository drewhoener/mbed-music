#include "mbed.h"
#include "I2S.h"
#include <cstdint>
#include <Speaker/Speaker.h>
#include "revkick.h"
#include "lokick.h"
#include "cyclon.h"
#include "key_reader.h"
#include "wave_generator.h"
#include "snare.h"
#include "rtos.h"

DigitalOut myled(LED1);
AnalogOut speakerAnalog(p18);
KeyReader reader1(p15, 523.25); //C3
Speaker speaker(p21);
Ticker sound_ticker;
Ticker input_ticker;
Wave generator = TriangleWave(440, GENERATOR_SAMPLE_SIZE);
I2S i2s(I2S_TRANSMIT, p5, p6, p7);

//AnalogIn reader(p15);

unsigned int i = 0;
double freq_buf[3];
int j = 0;
int buf_len = 1;
int buf[1];

double average(double[], int);
void on_distance(int);

//ultrasonic distance_sensor(p29, p30, .1, 1, &on_distance);

bool is_playing = false;

void advanceDrum(){

    //printf("%f\r\n", reader1.get_last_voltage());
    //buf[0] = generator.get_next_sample();
    //i2s.write(buf, 1);
    //buf[0] = square_generator.get_next_sample();
    //i2s.write(buf, 1);

    //speaker.PlayNote(440, 0, 1, 1);
    //speaker.StopPlaying();
    //return;

    if(is_playing && reader1.get_last_voltage() < .01599){
        is_playing = false;
        memset(freq_buf, 0, 3 * (sizeof(double)));
        j = 0;
        speaker.StopPlaying();
        return;
    }

    if(reader1.read_voltage() > .01599 && !is_playing && j < 3){
        //is_playing = true;
        freq_buf[j++] = reader1.get_frequency();
        return;
    }

    if(j == 3){
        if(freq_buf[0] != freq_buf[2]) {
            memset(freq_buf, 0, 3 * (sizeof(double)));
            j = 0;
            return;
        }
        is_playing = true;
    }

    if(reader1.get_last_voltage() > .01599 && is_playing){
        generator.set_frequency(reader1.get_frequency());
        //speakerAnalog.write(square_generator.get_next_sample());
        buf[0] = generator.get_next_sample();
        //speaker.PlayNote(reader1.get_frequency(), 0, 1.0/WAV_SAMPLE_SIZE, 1);
        //printf("Voltage: %f\r\nFrequency: %f\r\n\r\n", reader1.get_last_voltage(), reader1.get_last_freq());
        i2s.write(buf, buf_len);
    }
}

void tryDrum(){

    buf[0] = lokick_data[i];//scale down volume a bit on amp
    i++;
    i2s.write(buf, buf_len);//Send next PWM value to amp
    //printf("%d", i);
    if (i>= LOKICK_NUM_ELEMENTS) {
        i = 0;
        sound_ticker.detach();
        i2s.stop();
        printf("Stopping i2s\r\n");
    }

}

double read_buf[3];
int read_buf_len = 3;

void read_input(){

    //if(is_playing && )

}

void setup_play(){
    i2s.stereomono(I2S_MONO);
    i2s.masterslave(I2S_MASTER);
    i2s.frequency(GENERATOR_SAMPLE_SIZE);

    i2s.start();
    sound_ticker.attach(&advanceDrum, (float)1 / (GENERATOR_SAMPLE_SIZE));
    //sound_ticker.attach(&tryDrum, (float) (1.0 / (WAV_SAMPLE_SIZE)));
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
int main() {

    Thread sound_thread;
    sound_thread.start()

    printf("Frequency Map: ");
    for(int i = 0; i < 12; i++){
        printf("%f, ", reader1.get_freq_step(i));
    }
    printf("\r\n");
    i = 0;
    //wait(10);

    double average_one[5];
    double average_two[5];
    int counter_one = 0;
    int counter_two = 0;


    while(1) {
        //printf("Hello\r\n");
        wait(0.1);
        /*
        if(counter_one == counter_two){
            average_one[counter_one % 5] = reader1.read_voltage();
            counter_one++;
        }else if(counter_one > counter_two){
            average_two[counter_two % 5] = reader1.read_voltage();
            counter_two++;
        }

        //printf("Counter 1: %d\r\nCounter 2: %d\r\n\r\n", counter_one, counter_two);

        if(counter_one % 4 == 0 && counter_two % 4 == 0){
            //printf("Difference of Averages: %f\r\n\r\n", abs(average(average_one, 5) - average(average_two, 5)));
            myled = !myled;
        }
         */

        distance_sensor.checkDistance();
        //on_distance(distance_sensor.getCurrentDistance());

        //printf("Input Read: %f\r\n", reader1.read_voltage());
    }
}
#pragma clang diagnostic pop

void on_distance(int distance){
    printf("Distance: %d mm\r\n", distance);
}

double average(double* array, int length){
    //for(int i = 0; i < length; i++){
        //printf("%f, ", array[i]);
    //}
    //printf("\r\n");
    double sum = 0;
    for (int i = 0; i < length; ++i) {
        sum += array[i];
    }
    return sum / double(length);
}
