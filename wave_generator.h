//
//

#ifndef FINALPROJECT_SQUARE_GENERATOR_H
#define FINALPROJECT_SQUARE_GENERATOR_H

#include <cmath>
#include <arm_math.h>

/**
 * Based on code from
 * https://github.com/adafruit/Adafruit_ZeroI2S/blob/master/examples/tone_generator/tone_generator.ino
 * */

class Wave {
public:
    Wave(double frequency, int sample_rate) : frequency(frequency), sample_rate(sample_rate),
                                              amplitude(((1 << 14) - 1)), counter(0), samples() {

    }

    virtual ~Wave() = default;

    virtual int get_next_sample() {
        float delta = (frequency * samples_size) / float(sample_rate);
        unsigned int pos = int(counter * delta) % samples_size;
        counter++;
        return samples[pos];
    }

    virtual void set_frequency(double frequency) {
        this->frequency = frequency;
    }

    virtual void set_amplitude(int amplitude) {
        this->amplitude = amplitude;
    }

protected:
    double frequency;
    const int sample_rate;
    long counter;
    int amplitude;
    static const int samples_size = 512;
    int samples[samples_size];
};

class SquareWave : public Wave {

public:
    SquareWave(double frequency, int sample_rate)
            : Wave(frequency, sample_rate) {
        amplitude = ((1 << 11) - 1);
        for (int i = 0; i < samples_size / 2; i++) {
            samples[i] = -(amplitude / 2);
        }
        for (int i = samples_size / 2; i < samples_size; i++) {
            samples[i] = (amplitude / 2);
        }
    }

    ~SquareWave() = default;

};

class SinWave : public Wave {

public:
    SinWave(double frequency, int sample_rate)
            : Wave(frequency, sample_rate) {

        for (int i = 0; i < samples_size; i++) {
            samples[i] = (int) (float(amplitude) * sin(2.0 * PI * (1.0 / samples_size) * i));
        }
    }

};

class TriangleWave : public Wave {
public:
    TriangleWave(double frequency, int sample_rate) : Wave(frequency, sample_rate) {


        float delta = float(amplitude) / float(samples_size);
        for (int i = 0; i < samples_size / 2; i++) {
            samples[i] = -(amplitude / 2) + delta * i;
        }

        for (int i = samples_size / 2; i < samples_size; i++) {
            samples[i] = (amplitude / 2) - delta * (i - samples_size / 2);
        }
    }
};

class SawtoothWave : public Wave {
public:
    SawtoothWave(double frequency, int sample_rate) : Wave(frequency, sample_rate) {
        float delta = float(amplitude) / float(samples_size);
        for (int i = 0; i < samples_size; ++i) {
            samples[i] = -(amplitude / 2) + delta * i;
        }
    }
};

#endif //FINALPROJECT_SQUARE_GENERATOR_H
